#!env python3

import sys

def greet(who_to_greet: str):
    return 'Hello ' + who_to_greet


greeting = greet(sys.argv[1])
print(greeting)
